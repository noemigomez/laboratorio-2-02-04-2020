#!/usr/bin/env python3
# ! -*- coding:utf-8 -*-


import json


# apartado e
def e():
    # lista con keys
    k = ['Ranking', 'Cancion', 'Artista', 'Genero', 'Beat',
         'Length', 'Popularity']
    para_json = {}
    for j in range(len(k)):
        # lista de value
        v = []
        archivo = open('top50.csv')
        for i, linea in enumerate(archivo):
            linea = linea.split(',')
            # la linea 0 no aporta nada
            if i > 0:
                v.append(linea[j])
                """
                    Todos los indices de k coinciden con los de línea
                    menos el 6, ya que la popularidad es la columna 13
                """
                if j == 6:
                    v.append(linea[13])
        archivo.close()
        dic = {k[j]: str(v)}
        para_json.update(dic)
    # paso a json
    with open('top50.json', 'w') as file:
        json.dump(para_json, file)


# apartado d
def d(mediana, canciones, popularidad, ruido, art):
    mas_pop = 0
    # ciclo que da el mas popular
    for i in range(len(popularidad)):
        if mas_pop < popularidad[i]:
            ind = i
            mas_pop = popularidad[i]
    print('\nLA CANCION MÁS POPULAR ES {0}'.format(canciones[ind].upper()))

    if ruido[ind] < mediana:
        print('\nSe encuentra entre las menos ruidosas.')
        print('\nArtista | Cancion | Popularidad | Ruido')
        a = art[ind] + ' | ' + canciones[ind]
        a += ' | ' + str(popularidad[ind]) + ' | ' + str(ruido[ind])
        print(a)


# imprime datos solicitados en c
def imprimir_c(genre_bailable, canc_lentas):
    print('\nLOS GÉNEROS MÁS BAILABLES\n')
    for i in range(len(genre_bailable)):
        print(genre_bailable[i])
    print('\nLAS CANCIONES MÁS LENTAS\n')
    for i in range(len(canc_lentas)):
        print(canc_lentas[i])

# imprime datos solicitados en b
def imprimir_b(mediana, lista_mediana):
    print('La mediana es: {0}'.format(mediana))
    print('\nCANCIONES QUE TIENEN LA MEDIANA\n')
    print('Ranking | Canción | Artista | Género |')
    a = ''
    for i in range(len(lista_mediana)):
        for j in range(4):
            a += str(lista_mediana[i][j]) + ' | '
        a += '\n'
    print(a)


# imprime datos solicitados en a
def imprimir_a(artistas, repetidos):
    print('ARTISTAS EN EL TOP\n')
    for i in artistas:
        print(i)
    print('\nLOS ARTISTAS CON MÁS CANCIONES\n')
    for j in range(len(repetidos)):
        if repetidos[j] > 1:
            print('{0}, {1} canciones'.format(artistas[j], repetidos[j]))


# retorna generos más bailables
def bailables(genero, mayor):
    genre_y_mayor = []
    # junta genero con su bailabilidad
    for i in range(len(mayor)):
        genre_y_mayor.append([mayor[i], genero[i]])
    genre_y_mayor.sort()

    genre_bailable = []

    # las 3 mayores
    for j in range(len(genre_y_mayor) - 3, len(genre_y_mayor)):
        genre_bailable.append(genre_y_mayor[j][1])
    return genre_bailable


# retorna canciones más lentas
def lentas(canciones, menor):
    canc_y_menor = []
    # junta cancion con su tempo
    for i in range(len(menor)):
        canc_y_menor.append([menor[i], canciones[i]])
    canc_y_menor.sort()

    canc_lentas = []
    # las 3 menores
    for j in range(3):
        canc_lentas.append(canc_y_menor[j][1])
    return canc_lentas


# analiza y retorna canciones con menor tempo
def mas_lento(tempo, canciones):
    # menor a mayor
    for h in range(len(tempo)):
        tempo[h].sort()

    menor = []
    # el menor segun canción
    for i in range(len(tempo)):
        menor.append(tempo[i][0])

    canc_lentas = lentas(canciones, menor)
    return canc_lentas


# analiza y retorna los generos más bailables
def mas_bailable(dance, genero):
    # menor a mayor
    for h in range(len(dance)):
        dance[h].sort()

    mayor = []
    # el mayor segun genero
    for i in range(len(dance)):
        mayor.append(dance[i][len(dance[i]) - 1])

    genre_bailable = bailables(genero, mayor)
    return genre_bailable


# funcion que analiza si algo se repite en una lista
def revisar(a_revisar, lista):
    for i in range(len(lista)):
        if lista[i] == a_revisar:
            return False
            break
    return True


# apartado c
def c(genero, canciones):
    dance = []
    tempo = []

    # ciclo que crea una lista con los danceability
    for h in genero:
        cont_d = []
        cont_t = []
        archivo = open('top50.csv')
        for i, linea in enumerate(archivo):
            linea = linea.split(',')
            # la linea 0 no me entrega ningun dato de interés
            if i > 0:
                if h == linea[3]:
                    cont_d.append(int(linea[6]))
        dance.append(cont_d)
        archivo.close()
    # ciclo que crea una lista con los beat
    for h in canciones:
        cont_t = []
        archivo = open('top50.csv')
        for i, linea in enumerate(archivo):
            linea = linea.split(',')
            # la linea 0 no me entrega ningun dato de interés
            if i > 0:
                if h == linea[1]:
                    cont_t.append(int(linea[4]))
        tempo.append(cont_t)
        archivo.close()
    genre_bailable = mas_bailable(dance, genero)
    canc_lentas = mas_lento(tempo, canciones)
    imprimir_c(genre_bailable, canc_lentas)
    return genre_bailable, canc_lentas



# entrega lista con los datos de cada cancion con mediana ruido
def datos_mediana(mediana):
    lista_mediana = []
    archivo = open('top50.csv')
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        # la linea 0 no me entrega ningun dato de interés
        if i > 0:
            if mediana == int(linea[7]):
                # rank, cancion, artista y genero
                a = [linea[0], linea[1], linea[2], linea[3]]
                lista_mediana.append(a)
    return lista_mediana


# calculo de mediana
def medi(ruido):
    ruido.sort()
    mitad = int((len(ruido)) / 2)
    # retorna mitad - 1, ya que los indices comienzan en 0
    return ruido[mitad - 1]


# apartado b
def b():
    archivo = open('top50.csv')
    ruido = []
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        # la linea 0 no me entrega ningun dato de interés
        if i > 0:
            ruido.append(int(linea[7]))
    # calculo de mediana
    mediana = medi(ruido)
    # lista de todos los datos de canciones con la mediana
    lista_mediana = datos_mediana(mediana)
    imprimir_b(mediana, lista_mediana)
    return mediana, lista_mediana, ruido
    archivo.close()


# cuantas canciones tiene cada artista
def cantidad_canciones(artistas):
    repetidos = []
    for h in artistas:
        cont = 0
        archivo = open('top50.csv')
        for i, linea in enumerate(archivo):
            linea = linea.split(',')
            # la linea 0 no me entrega ningun dato de interés
            if i > 0:
                if h == linea[2]:
                    cont += 1
        repetidos.append(cont)
    return repetidos
    archivo.close()


# apartado a
def a():
    archivo = open('top50.csv')
    artistas = []
    # lista con artistas repetidos
    art = []
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        # la linea 0 no me entrega ningun dato de interés
        if i > 0:
            # si se repite el artista, False
            art.append(linea[2])
            ver = revisar(linea[2], artistas)
            if ver is True:
                artistas.append(linea[2])
            else:
                pass
    # cantidad de canciones en el top por artista
    repetidos = cantidad_canciones(artistas)
    imprimir_a(artistas, repetidos)
    return artistas, repetidos, art
    archivo.close()


# enlista cosas necesarias para ejercicio
def listas():
    popularidad = []
    canciones = []
    genero = []
    archivo = open('top50.csv')
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        # la linea 0 no me entrega ningun dato de interés
        if i > 0:
            popularidad.append(int(linea[13]))
            canciones.append(linea[1])
            # si el genero está repetido, False
            ver = revisar(linea[3], genero)
            if ver is True:
                genero.append(linea[3])
            else:
                pass
    return popularidad, canciones, genero


def evaluar():
    popularidad, canciones, genero = listas()
    artistas, repetidos, art = a()
    mediana, lista_mediana, ruido = b()
    genre_bailable, canc_lentas = c(genero, canciones)
    d(mediana, canciones, popularidad, ruido, art)
    e()


if __name__ == "__main__":
    evaluar()
